import secrets

from typing import Optional, Dict, Any
from pydantic import BaseSettings, AnyHttpUrl, PostgresDsn, validator, EmailStr, HttpUrl

from dotenv import load_dotenv


class Settings(BaseSettings):
    API_V1_STR: str = "/api/v1"
    PROJECT_NAME: str
    BACKEND_CORS_ORIGINS: str

    SECRET_KEY: str = secrets.token_urlsafe(32)
    ACCESS_TOKEN_EXPIRE_MINUTES: int = 43200

    SERVER_NAME: str
    SERVER_HOST: AnyHttpUrl

    DATABASE_TYPE: str
    DATABASE_SERVER: str
    DATABASE_USER: str
    DATABASE_PASSWORD: str
    DATABASE_NAME: str
    SQLALCHEMY_DATABASE_URI: Optional[str] = None

    @validator("SQLALCHEMY_DATABASE_URI", pre=True)
    def assemble_db_connection(cls, v: Optional[str], values: Dict[str, Any]) -> Any:
        if isinstance(v, str):
            return v
        if values.get("DATABASE_TYPE") == "postgresql":
            return PostgresDsn.build(
                scheme="postgresql",
                user=values.get("DATABASE_USER"),
                password=values.get("DATABASE_PASSWORD"),
                host=values.get("DATABASE_SERVER"),
                path=f"/{values.get('DATABASE_NAME') or ''}",
            )
        if values.get("DATABASE_TYPE") == "mysql":
            db_type = values.get("DATABASE_TYPE")
            user = values.get("DATABASE_USER")
            password = values.get("DATABASE_PASSWORD")
            server = values.get("DATABASE_SERVER")
            db = values.get("DATABASE_NAME")
            return f"{db_type}://{user}:{password}@{server}/{db}"

    FIRST_SUPERUSER: EmailStr
    FIRST_SUPERUSER_PASSWORD: str

    class Config:
        case_sensitive = True


load_dotenv(verbose=True)
settings = Settings()
