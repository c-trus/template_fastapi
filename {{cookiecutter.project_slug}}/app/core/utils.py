import random
import string


class Generator:
    @staticmethod
    def random_code(length):
        letters = string.digits
        return ''.join(random.choice(letters) for i in range(length))

    @staticmethod
    def random_string(length):
        letters = string.ascii_lowercase
        return ''.join(random.choice(letters) for i in range(length))
