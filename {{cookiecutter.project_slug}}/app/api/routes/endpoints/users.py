from typing import List

from fastapi import APIRouter, Body, Depends, HTTPException
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session

from app import crud, models, schemas
from app.api.dependencies import database, users

router = APIRouter()


@router.get("/", response_model=List[schemas.User])
def read_users(
        db: Session = Depends(database.get_db),
        skip: int = 0,
        limit: int = 10,
        current_user: models.User = Depends(users.get_current_superuser),
) -> List[schemas.User]:
    """Admin: List all users"""
    return crud.user.get_multi(db, skip=skip, limit=limit)


@router.get("/search", response_model=List[schemas.User])
def search_users(
        db: Session = Depends(database.get_db),
        skip: int = 0,
        limit: int = 10,
        email: str = "*",
        current_user: models.User = Depends(users.get_current_superuser),
) -> List[schemas.User]:
    """Admin: Search user by id"""
    return crud.user.search(db, skip=skip, limit=limit, field="email", look_for=email)


@router.post("/", response_model=schemas.User)
def create_user(
        *,
        db: Session = Depends(database.get_db),
        user_in: schemas.UserCreate
) -> schemas.User:
    user = crud.user.get_by_email(db, email=user_in.email)
    if user:
        raise HTTPException(
            status_code=400,
            detail="The user with this username already exists in the system.",
        )
    user = crud.user.create(db, obj_in=user_in)
    return user


@router.put("/me", response_model=schemas.User)
def update_user_me(
        *,
        db: Session = Depends(database.get_db),
        password: str = Body(None),
        current_user: models.User = Depends(users.get_current_user),
) -> schemas.User:
    """Update own user."""

    current_user_data = jsonable_encoder(current_user)
    user_in = schemas.UserUpdate(**current_user_data)
    if password is not None:
        user_in.password = password
    user = crud.user.update(db, db_obj=current_user, obj_in=user_in)
    return user


@router.get("/me", response_model=schemas.User)
def read_user_me(
        db: Session = Depends(database.get_db),
        current_user: models.User = Depends(users.get_current_user),
) -> schemas.User:
    """Get current user."""

    return current_user


@router.get("/{user_id}", response_model=schemas.User)
def read_user_by_id(
        user_id: int,
        current_user: models.User = Depends(users.get_current_user),
        db: Session = Depends(database.get_db),
) -> schemas.User:
    """Admin: Get a specific user by id."""

    user = crud.user.get(db, id=user_id)
    if user == current_user:
        return user
    if not crud.user.is_superuser(current_user):
        raise HTTPException(
            status_code=403, detail="The user doesn't have enough privileges"
        )
    return user


@router.put("/{user_id}", response_model=schemas.User)
def update_user(
        *,
        db: Session = Depends(database.get_db),
        user_id: int,
        user_in: schemas.UserUpdate,
        current_user: models.User = Depends(users.get_current_user),
) -> schemas.User:
    """Admin: Update a user."""

    user = crud.user.get(db, id=user_id)
    if not user:
        raise HTTPException(
            status_code=404,
            detail="The user with this username does not exist in the system",
        )
    if not (user == current_user or crud.user.is_superuser(current_user)):
        raise HTTPException(
            status_code=400, detail="The user doesn't have enough privileges"
        )
    user = crud.user.update(db, db_obj=user, obj_in=user_in)
    return user


@router.delete("/{user_id}", response_model=schemas.User)
def delete_user_by_id(
        user_id: int,
        current_user: models.User = Depends(users.get_current_superuser),
        db: Session = Depends(database.get_db),
) -> schemas.User:
    """Admin: Delete a specific user by id."""

    user = crud.user.get(db, id=user_id)
    if not user:
        raise HTTPException(
            status_code=400,
            detail="The user with this id does not exist.",
        )
    user = crud.user.remove(db, id=user_id)
    return user
