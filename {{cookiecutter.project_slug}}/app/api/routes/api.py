from fastapi import APIRouter

from app.api.routes.endpoints import authentification, users


api_router = APIRouter()
api_router.include_router(authentification.router, tags=["auth"])
api_router.include_router(users.router, prefix="/users", tags=["users"])